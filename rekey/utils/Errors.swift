//
// Created by nemopvt on 2018/03/17.
// Copyright (c) 2018 nemoto. All rights reserved.
//

import Foundation

enum RekeyErrors: Error {
    case fileLoadError(String)
    case filePathError
}
